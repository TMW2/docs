Welcome to Candor!
Our little small village gets enough supplies from Nard. We are not accepting new villagers. Trade is good.
 
If you have any suggestion write them at the end of the page.
______________________________________________________________________________________________________________________________
 
0. Role of Candor in the gameplay (META)
- It's the first island new players see, so:
- It has to catch the players to continue playing (neither too hard nor too easy)
- The first quests should be clear to understand
- Players of TMW Legacy and Evol should not get to think TMW2 is a cheap copy/ripof of them

TL;DR;
Rember:
The ship and Candor make the first impression so we should put much love into them - that we hook the players and they doesn't leave before it gets interessting.
In Case we need to focus on gameplay, musics, sfx, ambiance, weather, NPC activity and invite HIGH Level player to return to it

Map / Parts of the level

- What range of level is target? 0-12, 30+ for the cave
- What range of level is really practiced? 1-max Many because low prices in island

2. Gameplayability
- Player will be not locked by NPC at begining, 
- Remove instance from game start, to allow player to help their friendsd to start the game without creating a new char

3. Story
3.1 Candor Village Creation
In 368 After Tulimshar iirc, Saxso The Stranger With Arch Troup set location on Candor Island, after a ship crash they investigate this island and set life there, build houses, well, fence and add some plantation

3.2 Saxso The Stranger
Saxso build up first house on this Island, Some Arch Adventurer Set location with him 

3.3 Monster King Relation
-This village Barely not acknowledge by the Monster King, his minions sometimes make small attacks to score points. 
- During Candor siege people hides on the Crazyfefe Grave cave because magical shield.

3.4 Crazyfefe Grave Cave
3.4.1 History
3.4.2 Magic Place
3.4.3 Candor Battle
3.5 Arch Troup
A group of 20 Adventurers in quest of Finding a Mana Source, and figuring out the SECRET OF MANA.
Dark Secret: Children are an experiment, if exposition to such environment will create more mages without Mana Stone (full fledged Mages), all which are scarce.
3,6 Candor Economie
- At begining Candor citizen set out to be self-sufficient, but nowadays they trade ressources to Tulimshar via LaJohanne. they mainly buy stuff cause they don't have huge ressources production, Most of their ressource come from fees from Candor Battle evenment. 

//Saulc mess//
//A small Village.Founded by a traveller Called Saxso an Old mage from Frostia, this person know many things about Mana Source but since some years he is missing. no one found his body but citizen believe he dident die and some say they can saw his shadow.
Have wooden walls, a
Magic barrier come from Strange source of Magic, some people said its from old mage Called Saxso and some strange things apear when you proonce his anme
Do I need to add a listener? Sounds awful to code... Ugh  I cannot read this file anymore  A mess I leave
//



4. Scripts
4.1 Quest NPC's
Composed mostly by children, whose whom Ayasha watches over.
Security is provided by Morgan & Ayasha
4.1.1 Zegas
- Level required:
- todo: Check every barrel
-  Reward: {Candor Short]

4.1.2 Ayasha
- Level required:
- todo: Check every barrel
-  Reward:

4.1.3 Trainer
- Level required:
- todo:
-  Reward:

4.1.4 Candor Nurse
- Level required:
- todo:
-  Reward:

4.1.5 Saxso Chest
- Level required:
- todo:
-  Reward:

4.2 Shop NPC's

4.3 Extra NPC's
4.3.1 Soul Menhir
4.3.2 Cynril

4. Weather
Woodlands stanfstan

5. Maps
5.1 Outside
New Island tileset made by Saulc

5.2 Houses
5 to 7 small house where citizen live

5.3 Magic Lava Cave
Some strange maggic things appear there

6 Sound
6.1 Musics
6.1.1 Music Outside
Suggestion ?

6.1-2  Music Indoor
All should be same execpt for Magic Lava Cave
6.2 SFX
6.2.1 Elements SXF
Waterfall
Doors

6.2.2 Monsters SFX
Every monsters should have at least sound when they attack and die.

7. Monsters
Candor Island have his own bio-diversity
Sounds lots of wasted work but meh, we need new players 

A) Basic
Piou **unless we are changing, there is only normal pious
- Weakest monster of this Island
- HP : 85 Damage: 29
Maggots **unless we are changing, there is only normal maggots outside
- Next weakest monster, after Piou
- HP: 260 Damage: 36
Candor Scorpion
- HP : 500 Damage: 75
Scorpion
- HP: 950 Damage: 110
Candor Bee
- Candor bee replace Mana Bug, its monster with highest HP of Candor Island
Opposite. Mana Bugs can only show up in places like Candor. Even the Mana Stone fails in attracting them. I'll remove Hurnscald Mana bug
- HP: 2000 Damage: 65

B) Agressive
House Maggot
- Agressive monster who attack adventurer in group
- HP: 380 Dammage: 30 
Candor Croc
- Most dangerous Monster of this Island (that is in plain sight), Low level shoudl run away from it, in case its agressive but really slow
HP: 1200 Damage: 80
Lave Slime
- Hidden somewhere dark, much higher level than anything else on Candor... Aside from the Raid Event.
- HP: 4144 Dammage:

C) Boss (Agressive)
Saxso Ghost
- HP: 5000 Damage: 200

D) Inactive
Candor Bif
- This monsters need to be introduce by Rosen, Goal its to invite player to start mining
- Low spawn rate, good Coal and Iron Ore drop rate, really low gems drop rate
Candor Brush
- Some Foods can be obtain from it. Like Alface!
Clover Patch
- Try your luck to obtain  [Four Leaf Clover]
Candonut Tree
- Source of Candornut, watch the sky if you want to stay alive

SUGGESTION:
Palmtrees that can drop coconuts (maybe +rare chance of dealing damage when the coconut falls on your head)