# TMW2: Moubootaur Legends
## Jesusalva's ToDo List

## Moubootaur Legends Requests

* [ ] Politic Expansion
  * [ ] Town Event System (invest town GP)
  * [ ] Town Donation Box
  * [ ] Set bounties for sieges with town GP
  * [ ] Deploy barricades - next town siege easier
  * [ ] Guard Training - next town siege gives extra map EXP
  * [ ] Declare war against another town
* [ ] Alchemy: Add ascending recipes (specially ores)
* [x] Alchemy: Allow to repeat craft recipe (skip requestitem)
* [x] Basic Blueprints: Make them more available
* [ ] Intense Beard: Improve dialogs
* [ ] quests.xml - Add Blossom's and Thorn's Requests
* [x] Nard just rescued %s - Give them a warm @welcome at the game!
  * @welcome - Warps to Candor. Usable for 15 minutes after a new char
    registered themselves for the first time and completed the Tutorial Island
    (and started talking to Nard) Usable by any player with Candor Completion 60%.
* [ ] Random Dungeon Map Generator
* [x] Splash Damage - Ineffective or Harmful on 1v1 situations
* [x] Make mayors work to keep their town reputation
* [x] Make PF_HPCONVERSION a Mana Skill if needed, but add it.
    * Replace partly the SP regen from wands.
* [x] tmw2.itch.io : Mark “powered by Hercules and ManaPlus” on download list
* [x] Transmutation/Transmigration Interface
    * [ ] Boia/Bola Subsystem (D)
    * [ ] Evol Potions Subsystem (DD)
    * [ ] Water Subsystem (A)
    * [ ] Arrow Subsystem (A)
    * [x] Raw Coal Subsystem (-)
    * [ ] Gempowder Subsystem (R)
    * [ ] Herb Subsystem (R)
    * A: Ascending, D: Descending, DD: Double Descending, R: Random
* [x] Forgetful NPC (Magic Academy, free)
    * [x] T1
    * [x] T2
    * [ ] T3
* [ ] Assassin Archery
    * [x] Raise all bow range in +1 (or add the add_range passive skill)
    * [ ] Verify Explosive Arrow
    * [ ] Replace Bone Arrow with Explosive Arrow (Back-forth)
* [ ] Craft Recipes
    * [x] Weapons
    * [x] Shields
    * [x] Helmets
    * [x] Chestplates
    * [x] Pants
    * [x] Gloves
    * [ ] Shoes
    * [ ] Savior Armor
    * [ ] Craft NPC: Heavy/Light/Magic bonus set
        * Is this part of Savior Armor?
* [ ] Tent/Campfire
* [ ] PVP System and anti-PVP items
    * [x] Honor Score
    * [ ] PvP Imperial Arena
    * Near Frostia, to elect the PVP King, see #admin
* [ ] Balance Concerns
* [ ] Open Beta 2019 Bug List
* [x] Guild Badge System
    * [x] Guild Quest
    * [ ] Guild Bounty (GM)
    * [ ] Use variables instead of items?
        * What about guild-less bounty-hunters, then?
    * [x] Guild Badge Shop
    * [x] Max dungeon attempts: Guild Level?
    * [x] Badge Reward based on Guild Level? (1 badge/level)?
    * [ ] Guild - Warp object
    * [x] Guild - Kamelot Castle
    * [ ] Guild - GvG
* [ ] Ancient Blueprints on Ancient Desert (blame @Saulc I guess)
* [x] Main Story 6 and 7
    * [x] LoF Townhall
    * [ ] Lalica Arch
        * [ ] The Temple
        * [ ] Her Residence
        * [ ] The Showdown
        * [ ] Misc. Scripts
    * [ ] Airship Graphics (Bottom area, unless it is Tiled building)
    * [ ] Airship Landing Area
    * [x] Fortress Island
    * [ ] Intro Cutscene
    * [ ] Ancient Legacy (PS Ultimate)
    * [ ] Random Treasure and Abandoned Houses
    * [ ] Kingdom War System (reoutfit for GS-3)
    * [ ] Impregnable Fortress Boss Room (and the boss fight)
    * [ ] Final Boss and Cutscenes
* [ ] Gemini Assassins Quest
* [ ] The Academy
    * [x] Main Map
    * [ ] Storagehouse Maps
    * [x] Institute of Wizardry Maps
    * [x] Institute of Scholarship Maps
    * [x] Institute of Physical Sciences Maps
    * [ ] Student Quarters Office Maps
    * [x] Academy Administration Maps
    * [ ] Academy Forest Maps
    * [x] Main Class Redesign
    * [x] Subclass Redesign
    * [x] Magic Council Redesign
* [ ] Magic System version 3
    * [ ] Joyplim
    * [ ] Skill Unit to script System
    * [ ] Shadowclone (?)
    * [ ] Revive, Ressurect, Redemption
    * [ ] Sanctum, Imperial Seal
    * [ ] Brawling, Bear Strike, All In One
    * [ ] Sacrifice
    * [ ] Scripture
    * [ ] Liberation Strike
    * [ ] Weapon Elemental Adjustment Skills
    * [ ] Earth Skill Class
    * [ ] Assassinate
    * [ ] Arrow of: Devour, Urges, Ultima, Execution, Shatter,
          Advantage, Awakening, Flames
    * [ ] Horizontal, Diagonal, Vertical Slash, Stab, Grand Blast
    * [ ] NPC: Princess Of Night
* [ ] Mercenary Evolution
    * Convert 1* cards in 2* cards, etc.
* [ ] Fishing v2: Open Seas
    * [ ] Rowboat
    * [ ] Mechanics
    * [ ] NPC to allow it
* [x] Public Lightining System
    * [x] Add 'town light' NPCs
    * [x] Enable/Disable them along day/night announces
        * [x] Candor
        * [x] Tulimshar
        * [ ] Halinarzo
        * [x] Hurnscald
        * [ ] LoF Village
        * [ ] Lilit Island
        * [x] Nivalis
        * [ ] Frostia
        * [ ] World's Edge Village
* [x] Mercenary System Redesign
    * [x] Nivalis: Hire Mercenaries
    * [x] Nivalis: Buy Mercenaries
    * [ ] Nivalis: Upgrade Mercenaries
* [ ] Setzer
    * [ ] Monster Potion - dyed, special avatar
    * [ ] Monster Potion Item
    * [ ] Monster Potion Making at Nivalis
    * [x] Short Sword *somewhere* besides HH Store.
    * [ ] Pour the potion over the short sword... (How?)
    * [ ] Zambacutou
* [ ] Lilit
    * [ ] Real Estate
    * [x] Leather Trousers
* [ ] Mechanics
    * [x] Warp Crystals
    * [x] town.txt - Users coming back from death might need LOCATION$ reset
    * [ ] NPC_<Ele>ATTACK to all relevant monsters (overpopulating mob skill db)
* [ ] Monsters
    * [ ] Spider
    * [ ] Flowers
    * [x] Cursed Soldiers (Micksha)
    * [ ] Duck King
    * [ ] Gatekeeper
    * [ ] Hydra Cat
    * Concepts:
    * [ ] THE MOUBOOTAUR
    * [ ] Scorpion King
    * [ ] Mechanical Beigns (Assault Weapons, (Mobile) Fortifications, Siege Crossbows...)
    * [ ] Sea Monsters (Hydra, Tentacles, Octopus, Sea Snake, Tritans, Nagas, Merfolk, etc.)
    * [ ] Mimics?
    * [ ] Generic Cave Monster (strange creatures without counter-part on real world)
    * [ ] Harpies, Eagles, Birds, Gryphons, etc.
    * [ ] Magical beings: Elementals (Fire elemental, Water elemental, etc.), Summon
          Gates, Homunculuses, Woses, Golems (small, big, fat, thin, with swords, with lazers, etc.) ...
    * [ ] Insects and Insectoids (eg. ants, bees, annoying flies and swarms, lizards,
          Alligators, etc.)
    * [ ] Ninjas, Gladiators, etc. for the PvP arena - It should work, if we only
          have one player for the PVP Duel, no need to cancel the fight - just spawn
          a few Gladiator monsters and enjoy :>
    * [ ] Dragons (Red Dragon, Smoke Dragon, etc. - Green Dragons being the weakest)
        * [ ] Wyverns and Drakes can be done, too

* [ ] Other
    * [ ] Katzei Family
        * [ ] Black Cat Boots ( serverdata#47 )
        * [ ] Cat Ears ( serverdata#47 and LOW PRIORITY )
    * [ ] Block @spawn of Moubootaur or Monster King
    * [ ] Automatic Treasure Chest monster spawn in dungeons (maybe with an event so
          previous spawns are killed each cycle, and population is kept roughly based
          on player count) - Or mimic :> Feeling lucky? :>
    * [ ] Traps, poison makers for poison arrows: Buy common Arrow, and ask a Poison
          Maker Player to upgrade them! (Make normal arrows cheaper, too)

* [ ] Experience Orb items
    * Treasure chests drop those?
* [ ] Tulimshar's Legacy Quest
    * Red (Crimson) Knights and the Red Queen
    * Time Flask warp target
* [x] Automatic Daily Event System (FY:DES)
    * Never repeat same event on same month (or maybe, same season)
      * `array_shuffle` at creation + `array_pop` until empty?
      * Or should it just go linearly? (gettimeparam(WEEK) % getarraysize())?
    * Last ~~1~3 days~~a whole week
    * Excessive Randomness, specially on dialogs and settings ($@_$-abuse) for an unique experience
    * Example: Map EXP Bonus of 25%
    * Note: Might be interesting to do like TMW2:Spheres and have they weekly (using a rotating list of events/rewards with sighly tweaked lore, and mostly Strange Coins as rewards?)
* [ ] "Impossible" tasks
    * [x] MyRand
    * [ ] HUSS
    * [ ] gmauth: Discord/IRC
    * [ ] Rain (Weather) should have an effect ingame
* [ ] Party Dungeon Second Floor
    * Use setcells() and switches to close the wings
    * Add a step portal on the bottom of each wing. Player must be standing on it.
    * If a player is standing in every portal, all players get warped.
    A way to warp more is to everyone pile up at one portal, or to divide in equal-size
    teams
    * [ ] Third floor, I don't care anymore at this point
    * [ ] Release it!
* [ ] Extend Khafar
* [ ] Extend Agostine
* [x] LoF Bot quest to visit LOF
* [ ] Cooking
* [x] Marggo: Use the chance to explain Alcohol Boosters
* [ ] Finish True Buccanner Quest
* [ ] Pyndragon Extended Weapon Selection
    * [x] Lightsaber: Fast 2 hand staff (SETZER speed)
    * [x] He probably will inheirt Setzer as well
    * [ ] Techno-Wands specialized in raw attack speed and power
    * [ ] Single Handed Sword already specialized in splash
    * [ ] Power-Wand which autocasts lightning bolts (but is SLOW and eat mana).
    * [ ] Thief's Sword, which steal items and may even collect loot! (Maybe)
* [x] Elanore - Support for batches of lifestones
* [ ] Increase spawn rates
    * [x] Tulimshar
    * [ ] Hurnscald
    * [ ] Desert Canyon
    * [ ] Lilit
    * [ ] Ice Fields
    * [ ] Cindy's Cave Complex
    * [ ] Frostia Outskirts
    * [ ] ...The world is huge...
* [x] Kreist over-the-requested reward
* [x] Add a tutorial text to the skull warning on Saxso's House
* [x] What about the nurse giving the slippers for donating blood 5 times? "Now you must feel homelike already here.. take these slippers so your feet keep warm!"
* [x] Terranite (sp) Armor
* [x] Heavy Armor and Heavy Helmets - MaxHP bonus needs update
* [x] Armor DEF on items.xml is outdated (too many armor for a too lazy Jesusalva)
* [ ] Add chatlog cleaner at OnClock0500
    * [ ] Remove only logs older than 3 months
    * [ ] Optionally, @ucp option to clear your own logs older than 7 days?


----

# Archived Old Still Relevant ToDo lists

## Mapping Requests
* [ ] Random Dungeon Map Generator
* Fortress Island Maps
* [ ] Airship Landing Area
* [ ] The Impregnable Fortress (maybe 10 floors is enough?)
* [ ] Impregnable Fortress Boss Room (and the boss fight)
* Academy Maps
* [ ] Storagehouse Maps
* [x] Institute of Wizardy Maps
* [ ] Institute of Scholarship Maps
* [x] Institute of Physical Sciences Maps
* [ ] Student Quarters Office Maps
* [ ] Academy Administration Maps
* [ ] Academy Forest Maps
* Lilit
* [ ] Lilit Central Tree (Upwards Climbing)
* [ ] Lilit Throne Room
* Existing Maps
* [ ] Fortress Town
* [ ] Hurnscald Pirate Caves
  * [ ] Pirate monsters
* [ ] Katzei the Cat Cave
* [ ] Heroes Hold final levels
* [ ] Iilia Maps

## Game Lore
* [x] Next Main Storyline movement
    * Monster King and the fragment hunt: Script control that
    * [ ] Main Continent must be finished and more mobs must be added before going to
    his Stronghold and attempt to take it down - design so a team of five players
    level 109 with Savior Set can handle the task.
* [x] Next Player Storyline movement
    * We'll start using Karma System for real, and give you the three legacies based
    on that and affected by player choice.
    * Next movements are building Karma points, using main storyline frame.
    * [x] Lua is a good NPC if you like the "open world" style, but add the guard NPC...
        * [x] And then, use this Guard NPC to sort player priorities
* [ ] Next City Development
    * Nivalis still needs to be finished with Angela and Blue Sage.
    * This will move city development to Frostia and Lilit next.

### Real Estate System
* [x] Add a house in LoF Village
* [x] Mirror that house multiple times
* [x] Add a house in Halinarzo too (cheapest ingame)
* [x] The castle in the Road
* [ ] Fairy house
* [x] Apartment Building
* [x] Sanitize rent prices

### Minigames
* [ ] Merchant Police Minigame
    * [ ] Strengthen lock: by melting an iron or copper ingot
    * Minigame to don't bust the lock (the right melting temperature)
    * Adds money to the safe and gets JOB EXPERIENCE (1% from needed to level up)
    * [ ] Run Bussiness: get a share from revenue and JExp
    * Similar to classic serve-clients-style minigame
    * Memorize client requests (take prints) - all time in the world
    * clear; is called and must select right option - small time frame
    * Super easy to cheat, but it is revenue based - must then wait 12 hours.
    * Might require a Quill - maybe?

### Balance System
* [ ] Review refine prices?

### Mapping
* [ ] Pirate Cave
    * **OF COURSE** I mean TMW-BR Pirate's quest. And of course it is in hurscald.
    Saying more than that is spoilers, so I'll stop now. Unless Saulc have ideas.
* [ ] Racing Maze
    * [ ] For Monsters
    * [ ] For Players and NPCs

### Magic Development
* [ ] Lalica the Witch (reward: Hat)
* [x] Super Attack Boost fake-skill BERSERK (attack speed +100% SOME MALUS)
    * [ ] Add that skill somewhere “Divine Rage” (TMW2_DEMURE)

### Dungeon Development
* [ ] Dungeon Fishing
    * Add more spots
    * Legendary Savior Set Blueprints
* [ ] We need a minecart in some dungeon (travel between cave chambers) in a Party Dungeon.
    * Or we could make it like Rush Game. Eh, board games are tricky.
* [x] Global Boss
    * Defeating it could, as Saulc said, raise server EXP rates for a while.
    By setting $@EXP_EVENT to rad(5,15), `monster(); donpcevent "@exprate::OnPlayerCall"`.
* [ ] Heroes Hold
    * Traps should cause status effects at random too (these need client-data patch)
    * Add the missing levels for Crazyfefe if needed
* [ ] Grand Race
    * Objective is to reach first the other side of a maze - you're running against
    players AND NPCs (which have a set of random pathes they'll take). All equipment
    is allowed and skills are all green. GMs cannot participate.
    * This means if the race is hourly, even if nobody else is interested,
    there still are NPCs running and you may lose to them
    * You could bet Casino Coins too :3 Bet on your friend! Get more Casino Coins!

### LILIT, THE FAIRY ISLAND
* [x] The access bridge
* [x] The spider cave
    * must be mapped
    * spiders must be added
* [x] Boss Fight at the end (Level 40, multiplayer)
* Monsters
    * Alizarin Herb
    * Fairies
    * Green Dragons at middle
    * Mountain Snakes at bottom
* Quests and NPCs
    * [ ] Daily for Mountain Snake Egg
    * [ ] Permanent - Armor - for 40x Mountain Snake Skin
    * [ ] Fairy Pet - see Horcrux in BR for reference (haaaaaaaard, like Crazyfefe likes)
    * [x] Grand Hunter Quest - Mountain Snake (might take it while doing other quests)
    * [ ] Quest with Mountain Snake Tongue and Dragon Scales I guess?
    * [x] Leather Pants
* [ ] Lilit Central Tree
    * Player groups (lv 40) might want an audience with Lilit, thus, climing the tree.
    * They have to climb the stairs (ie. move upwards).
    Mobs are created as players progress. The mob is aggressive.
    * If players reach the top, they'll be brought to Lilit audience room.
    * [ ] Lilit challenge players to the Snake Pit if they want (like Crazyfefe Cave,
        but monster is fixed - Mountain Snake - with 1 extra snake every turn)
    Must kill all to advance round, and there's a reward at end (5+players*2 rounds?)
    It is a PARTY INSTANCE with warpparty().
    * [ ] There's also a General there. So you can challenge the Yetifly.
    * Real Estate booking in Lilit is managed here, of course.
    * Diplomacy is welcome

### FROSTIA, THE FROZEN KINGDOM
* :questionmark: More elves quests so you can enter while suffering racism.
    * Cindy quest should not be a pre-requisite.
* [x] Assassin Set quests are ready - just copy paste
* [ ] Red Stockings
* [x] AF King Arthur
* [x] Replace Desert Bow by Elfic Bow and make it available.
* [ ] Challenge Frostia King to a duel with any PARTY. Causes a server cooldown.
    * No reward is provided. In other words, killing the king reward is suffice.
    * Or actually, we might give players some solid EXP boost, like a true boss.
* [ ] Other and any level 70+ content when appliable.

### Other places
* [ ] Redy's Volcano?
* [ ] Oceania?
* [ ] Orc Nomad Village? (Barbarians?)

#### Other Quests
* [ ] Inspector Quest (reward: Inspector Hat)
* [ ] Rock-Paper-Scissors (Halinarzo, probably casino)

#### Other items
* [ ] Jeans Chaps
* [ ] Monster Potion
* [ ] Setzer background story
* [ ] Fate's Potion
* [x] Caffeinne - Cures Sleep (and ...?)
    * Item description: John H's favorite.

##### Missing Level 40~70 Equipment
* [ ] Setzer
    * Must learn about monster potion on Khafar
    * Monster potion is done at Nivalis Potion Shop
    * Must learn Setzer's Backstory «WHERE? WHICH ONE?»
    * Can be craft «WHERE? BY YOURSELF?»
* [x] ????????
    * We need a new two hands weapon for level 87 and 105, low priority, just for record
* [ ] Warlord Boots
    * Part of Warlord Set - Nahrec?
* [ ] Jeans Chaps
    * Shouldn't be Pashua, given Canyon Difficulty (level 56)
    * Requires Jeans Shorts
* [ ] Silk Pants
    * Magic Equipment, Tulimshar
    * Baktar wants one, too
* [ ] Copper Armor
    * Part from Lieutenant set - but which one? Hmm, @Saulc could help perhaps?
* [ ] Warlord Plate
    * Part of the Warlord Set - Nahrec?
* [ ] Crusade Helmet
    * Colonel DUSTMAN is sitting in one!
* [x] Prsm Helmet
    * (Actually, shouldn't require Terranite Ore, Warlord Helmet, or be Spring only)
    * Paxel and his dialog are nice and dandy, but quest difficulty is stupid
* [ ] Golden 4 Leaf Amulet
* [ ] Platinum 4 Leaf Amulet
    * Requires (previous 4l amulet) and a real 4 leaf clover this time (besides platyna)
    * Supreme boost, finally
* There's stuff on Art Repo which wasn't taken in account.
    * Lack of notes like this one means lack of plans properly recorded somewhere.

#### Minor
* [ ] Treasure Chests animation
* [ ] Add story books to Halinarzo Library
* [ ] Iilia Quest
    * [ ] Isbamuth
* [ ] Golden and Titan monsters
    * Stronger variation of common monsters with more rewards/harder to slay.
    * Golden is focused on drops, and Titan is focused on EXP.
* [ ] Discuss with Saulc about Nahrec the Heavy Armor Maker and about Warlord set
    * Bring the second helmet from Warlord Set from TMW-BR too, very good quest
* [ ] We could use real money...

