#Travel	
## This page list suggestions to travel around TMW2 maps
	
* By ship (must pay for it)
  + Fee is based on number of completed quests, ranging from 250 to 2000 GP.
* by magic travelers (must pay and required to speaked before to the traveler you want to visit)
  + <Jesusalva> We do not really need it, imho
* by magic spell ( that consume items+ cooldown))
  + <Jesusalva> TMW-BR had consumable Crystals which could be used to warp. We could use something similar.
* By magic towel to soul menhir (cooldown)
  + <Jesusalva> Maybe we do not need a Towel, it can be other item. It must be very difficult to find. I also suggest using mana, if possible ^^
* By love teleportation (only possible in mapflag town + long coooldown required to be maried to our partner adn outr partner need to accept the flee)
  + <Jesusalva> This is already a skill. Lacks testing, though.

