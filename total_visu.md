# Total Visualization

This file was generated automatically and is rather messy.
It is not effective anymore, please refer to [RoadMap](roadmap.md) to see what's left from it.
Check also [EleGen](https://tmw2.org/EleMonsters) and Item Gen (TODO) and/or [Xtreem Visu](https://tmw2.org/xtreem) - the former one isn't maintaned anymore.

The major disadvantage of MarkDown is that we can't use colors. Argh.

|Level|Pant|Rarity|Stats|armor|Rarity|Stats|Hat|Rarity|Stats|helmet|Rarity|Stats|boot|Rarity|Stats| |Rarity|Stats|shield|Rarity|Stats|sword|Rarity|Stats|bow|Rarity|Stats|charm|Rarity|Stats|amulet|Rarity|Stats|rings|Rarity|Stats|ammo|Rarity|Stats|neck|Rarity|Stats||stat helmet||
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|1|creassed|Quest|10|creassed|Quest|12|GM ADM DEV|GM|1|||||||crassed |quest|5|barrel|||rusty knife|quest|25|training|buyable|20|bronze med|event|1||||weeding|buyable|1|tolchi|buyable|10||||30|29||
|2|miniskirt|Craftable|15||||bunny|drop|5|||||||||||||toy sabre|exotic|1||||silver med|event|2|||||||||||||22|17| |
|3|candorshort|Quest|18|short tank top|craft|20|||||||creased|quest|4|||||||small knife|quest drop|35||||golden medal|event|3|||||||||||||45|45||
|4||Quest - Buyable|25|candor shirt|quest|20|axe|halloween quest|8|||||||||||||knife|quest|50|||||||||||||||||||53|45||
|5|skirt|Buyable|25|||||||||||||cotton|quest|19|round leather|Buyable|35|sharp knife|quest|70||||broken doll|drop|1|toothnecklace|quest|1|simple|buyable quest|2||||moustache|quest|1|84|83||
|6||||tank|Quest - Craftable|40|rose|quest|9||||||||||||||||||||||||||||||||||49|40||
|7|||||||knit|quest|20||||candor|quest|6||||||||||||||||||||||||||||26|6||
|8||||||||gm event|30||||||||||||||||||||||||||||||||||30|0||
|9|||||||candor headband|quest|18||||||||||||||||||||||||||||||||||18|0||
|10|||||Buyable - Quest||murderer crown|event |35||||loosy|shop|10|||||||wand|quest|25||||doll|quest|2|lifestonependant|craft|2||||training|buyable|20|cashiershade|quest|2|51|14||
|11|||||||silk head||30||||||||||||||||||||||||||||||||||30|0||
|12|||||||bandana|quest|26||||||||||||||||||||||||||||||||||26|0||
|13|cotton short|quest|25||||Antlers|drop|32||||||||||||||||||||||||||||||||||57|25||
|14||||xmas|Quest|60|||||||cotton|quest|12||||||||||||||||||||||||||||72|72||
|15|||50|cotton shirt|quest|30|serf|quest |38||||||||||leather|quest|55|dagger|quest|100|small bow|quest|50|leatherball|quest|3||||||||||beard|quest|1|177|138||
|16|||||||pinkie|drop|25||||||||||||||||||||||||||||||||||25|0||
|17|||||||cap|exotic|35||||||||||||||||||||||||||||||||||35|0||
|18|||||||||||||tulimshar guard|quest|16|||||||deadhand|drop|98|||||||||||||||||||16|16||
|19|||||||fancy|drop|42|bowler|||||||||||||||||||||||||||||||||42|0||
|20|Torm Pirate Short|drop|35+1evade|silk robe|shop|45 +40sp|Eye Patch||1|ranger|quest|50|boot|drop quest|30|||||||wooden |quest|130||||spectrall orb|drop|4|woolverntooth|quest|3|Silver|||arrow|craft|30|automn mask|automn|5|#VALOR!|#VALOR!||
|21||||leather|drop|58|piou headband|quest +1dex|26||||||||||||||||||||||||||||||||||84|58||
|22|||||||Fluffy|drop|39||||||||||||||||||||||||||||||||||39|0||
|23||||t neck|quest |100|green eggshell|easter|55|||||||||||||maceblub|craftable|180|||||||||||||||||||155|100||
|24|||||||sailors|quest drop|40||||||||||wooden|quest|72||||||||||||||||||||||112|72||
|25|jeans shorts|drop|49|Community Shirt|quest|40|desert hat|buy|40|paperbag|??? Gm event|47||||||||||bug slayer|quest|200||||redstocking|winter|5|monk pendant|quest|4|light ring|quest|3||||sunglass|summer|5|146|148||
|26|||||||shroom|drop|54||||||||||||||||||||||||||||||||||54|0||
|27|||||||mush|drop|55||||||||||||||||||||||||||||||||||55|0||
|28|||||quest||pirate bandana|drop|36+10atk|||||||||||||||||||||||||||||||mana eyes||10|#VALOR!|#VALOR!||
|29|||||||miner|quest |44|pumpkin|drop|66|||||||||||||||||||rawtalisman||||||||||||44|66||
|30|Luffy Short|Quest|55+2atk|yety|quest|150|darrk egg|easter|60|R Eye Patch|Dayli||leather|quest|45|mineur||25|||85|training gladius||250|short bow||120|four leaf clover||6|||5||||iron||40|shemagh||10|#VALOR!|#VALOR!||
|31|||||||mouboo||||||||||||blade|quest||hatchet||280||||||7|||||||||||||7|7||
|32|||||||chef miner||75|||||||||||||scythe||500|||||||||||||||||||75|0||
|33|cotton trouqers|quest|59||||brimmed hat||55||||||||||||||||||||||||||||||||||114|59||
|34|||||||||75||||||||||||||||||||||||||||||||||75|0||
|35||||Sailors shirt|quest|55 + 1 str|infantry||120|||||||leather armband||30||||butcher||350||||snowman|||dark pendant||6|||||||rednose||5|#VALOR!|#VALOR!||
|36|||||||bucket|quest|50|||||||||||||baselard||430|||||||||||||||||||50|0||
|37|||||||||||||||||||||||||||||||||||||||||||0|0||
|38|||||||corsair|||||||||||||||bronze gladius||500|||||||||||||||||||0|0||
|39||||||||||prsm||100|furs|quest|55||||braknar|quest|105|short sword||580||||||||||||||||snow google|||160|260||
|40|reid trousers||||||fairy||42+1dex|||||||leather||22|||||||wooden|quest|70||anta|8|crozenitefour||7|||4|cursed||50|googles|str coins|6|#VALOR!|#VALOR!||
|41|||||||nut||90|||||||||||||||||||||||||Golden Ring||||||burglar mask |||90|0||
|42||||desert shirt|quest|74|Circlet||45+20sp|eggs|quest drop|95|||||||||||||||||||||||||||||||#VALOR!|#VALOR!||
|43|||||||noh|||yety|drop|65||||||||||guard||620||||||||||||||||||15|15|65||
|44|||||||||50|||||||||||||backsword||680|||||||||||||||||||50|0||
|45|bandit pant|Drop|90|light plate||260|Pan|Quest|60|bromenal|craft drop|150|redstolking|qesust|38|silver ||40|brit|event ? Quest|120|||||||earth scroll||9|jacknecklace||8|||||||leather google||5|630|715||
|46|||||||elfnightcap||||||||||||||||||||||||||||||||||||0|0||
|47|||||||wolen||60||||||||||||||||||||||||||||||||||60|0||
|48||||Forest armor|quest |90 +1dex|trapper||65||||||||||||||||||||||||||||||||||#VALOR!|#VALOR!||
|49|||||||||99|||||||||||||rapier||510|||||||||||||||||||99|0||
|50|leather trousers|Quest|100|bromenal|craft drop|280|graduation cap|quest|27|candle||160|bromenal|craft drop|62|silk||50|ancient|rare drop|190|bone||730|forest ||130|fire scroll||10|claw pendant||9|theta rings||5|poison||60|google||5|738|866||
||1|||||||||||||||||||||||||||||||||||||||||||||
|51|||||||||||||||||||||||||||||||||||||||||||0|0||
|52|||||||demon|||desert helm||190||||||||||woodenstaff||150||||||||||||||||||18|18|190||
|53||||||||||||||||||||||knight||790|||||||||||||||||||0|0||
|54|||||||||||||||||||||||||||||||||||||||||||0|0||
|55|jean chaps|Quest|120|moonshroom||110|catears||15|alchimist||150|wisard||22|forest||38||||ancient sword||900||||demonashurn||11|iron four leaf||10|||||||achilist google||9|335|461||
|56|||||||||||||||||||||||||||||||||||||||||||0|0||
|57|||||||||||||||||||||||||||||||||||||||||||0|0||
|58|||||||bennie copter||60|||||||||||||firesword||750|||||||||||||||||||60|0||
|59|||||||||||||||||||||||||||||||||||||||||||0|0||
|60|silk pants|Quest|80|warlord|craft drop|350|hight press crown||150|||||craft drop|75|bromenal||60|bromenal|drop|160|ice gladiadus||750|crusader||190|grimoire||12|bloodstone||11|rings stats||6|thrnarrow||70|heart glasse||8|912|754||
|61||||valentine|annual quest|130||||crusade||210||||||||||staff of ice||160|||||||||||||||||||130|340||
|62|||||||wisard||60|||||||||||||halbard||1000|||||||||||||||||||60|0||
|63|||||||||||||||||||||||||||||||||||||||||||0|0||
|64||||||||||||||||||||||bromenal||850|||||||||||||||||||0|0||
|65|chainmail|Quest|190|assassin||200|chritmas||75|warlord|craft drop|250|black ||50|golden||70|blue knight|quest|170|setzer||820||||||13|barbarian||12|||||||||19|799|955||
|66|||||||guy||||||||||||||||||||||||||||||||||||0|0||
|67|||||||mouboohead||111||||||||||||||||||||||||||||||||||111|0||
|68||||||||||||||||||||||deheader||900||||plush mouboo||14|||||||||||||14|14||
|69||||||||||underworld||125|||||||||||||||||||||||||||||||0|125||
|70|bromenal|Drop - Craftable|210|golden light||280|moonshroom||100||||witch||26|warlord||82|crusade|quest||rock||850|iced||200|theta||15|bromenal||13|goblack pearl||7|bone arrow||80|right eye patch||2|735|633||
|71||||||||||||||||||||||staffoffire||175|||||||||||||||||||0|0||
|72|||||||||||||||||||||||||||||||||||||||||||0|0||
|73||||||||||||||||||||||indian spear||1200|||||||||||||||||||0|0||
|74|||||||sorcerer||75||||||||||||||||||||||||||||||||||75|0||
|75|warlord|Quest|390|golden warlord||360|eye||5|monster skull|drop buyable|200|helios||10|mana||40|steel|quest|200|long sword||900|desert||250|demonscull||16|fighttalisamn||14|||||||||25|1060|1230||
|76|||||||skull|||jeeaster||95|||||||||||||||||||||||||||||||0|95||
|77|||||||||||||||||||||||||||||||||||||||||||0|0||
|78||||||||||doctor||95|terranite||59|||||||trident||1300|||||||||||||||||||59|154||
|79|||||||goblin|||Terranite|quest|110||||||||||||||||||||||||||||||28|28|110||
|80|terranite|Quest - Drop|350||||left eye patch|||||||||terranute||59||||axe||1060||||oldtowel||17|barbarian master||15|valentine||8|terra ar||90|monocle||11|460|449||
|81||||terranite|quest|250|||||||||||||||||||||||||||||||||||||250|250||
|82||||||||||pinki helm||112|||||||||||||||||||||||||||||||0|112||
|83|||||||||||||assassin||60|||||||warlord||1200|championship||300||||||||||||||||60|60||
|84|||||||face||||||||||||dragon|craftablke|||||||||||||||||||||||0|0||
|85|assassin|Quest|300|sorcerrer||150|brown bowler||52|dark helm||290||||scarabet||60||||demonic scythe||1350||||astral cube||18|golden four leaf||16|||||||||12|608|834||
|86|||||||||||||||||||||||||||||||||||||||||||0|0||
|87|||||||witch||85|paladin||310|||||||||||||||||||mistical ||17||||||||||102|327||
|88||||||||||||||||||||||staffof life||180|||||||||||||||||||0|0||
|89||||lazurite||165||||blinkingmask||| ||||||||||||||||||||||||||||||165|165||
|90|savior|Quest|420|savior||420|opera||55|bull||335|warlord||80|assassin||65||||divine||1460|banshee||400|heart of isi||19|heart necklace||18||||divine arrow||100|||30|1107|1357||
|91|||||||||||||||||||||||||||||||||||||||||||0|0||
|92||||||||||overlord||350|||||||savior|quest|250||||||||||||||||||||||250|600||
|93||||||||||||||||||||||divine staff||200|||||||||||||||||||0|0||
|94|||||||||||||||||||||||||||||||||||||||||||0|0||
|95|||||||crown||100|golden monster||370|savior|||||||||falchion||1550||||golden heart of isi||20|angel||19||||||||||139|409||
|96|||||||||||||||||||||||||||||||||||||||||||0|0||
|97|||||||||||||||||||||||||||||||||||||||||||0|0||
|98|||||||||||||||||||||||||||||||||||||||||||0|0||
|99||||||||||golden bull||370||||||||||golden long s||1600|||||||dark talisman||20||||||||||20|390||
|100|18|||26|||53|||26|||16|||15|||11|||45|||10|||23|||20|||8|||10|||22|||0|0||
|||||||||||||||||||||||||||||||||||||||||||||||
|||||||||||||||||||||||||||||||||||||||||||||||
||to add next release|||||||||||||||||||||||||||||||||||||||||||||
||implemented quest item|||||||||||||||||||||||||||||||||||||||||||||
||implemented drop|||||||||||||||||||||||||||||||||||||||||||||
||BROKEN or with prob|||||||||||||||||||||||||||||||||||||||||||||
||Shop|||||||||||||||||||||||||||||||||||||||||||||
||Special-GM-Strange Coin|||||||||||||||||||||||||||||||||||||||||||||
|||||||||||||||||||||||||||||||||||||||||||||||
||When an item is drop by monster and can be optain in quest |||||||||||||||||||||||||||||||||||||||||||||
