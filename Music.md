# Music
## This page lists game soundtracks
## and where they show up or should show up

| Music | Location |
| ----- | -------- |
| 3b5.ogg | Canyon Theme |
| 8bit_the_hero.ogg | Nivalis/Frostia Indoor |
| academy_bells.ogg | Academy Island Theme |
| Arabesque.ogg | Action Theme |
| bartk_adventure.ogg | **UNUSED** |
| caketown.ogg | Hurnscald Theme |
| dance_monster.ogg | Quest Theme |
| dariunas_forest.ogg | Sincerity/Woodlands Theme |
| Deep_Cave.ogg | Cave Theme |
| dragon_and_toast.ogg | Halinarzo Theme |
| eric_matyas_ghouls.ogg | Cave Theme |
| eric_matyas_surreal.ogg | Cave Theme |
| icecave.ogg | Nivalis/Frostia Action Cave Theme |
| let_the_battles_begin.ogg | Boss Theme? |
| magick_real.ogg | Candor Indoor Theme |
| Misty_Shrine.ogg | Mana Temples/Shrines Theme |
| misuse.ogg | Monster King Theme |
| mvrasseli_nochains.ogg | Tulimshar Theme |
| mythica.ogg | GM Theme |
| New_Woodlands.ogg | Woodlands Theme |
| peace.ogg | Indoor Theme |
| peace2.ogg | Indoor Theme |
| peace3.ogg | Academy Indoor Theme |
| Ruins.ogg | Cave Theme |
| sail_away.ogg | Ship Theme |
| school_of_quirks.ogg | Candor Theme |
| sidequests.ogg | Fortress Island Theme |
| snowvillage.ogg | Frostia Indoor/Cave Theme |
| steam.ogg | LoF Theme |
| tmw_adventure.ogg | Lilit Theme |
| tws_birds_in_the_sunrise.ogg | Soren/Nivalis/Frostia Theme |
| tws_green_island.ogg | Mana/Sactuary Theme |
| Unforgiving_Lands.ogg | Swamp Theme |
| water_prelude.ogg | Prologue/Iced Theme |
| woodland_fantasy.ogg | Hurnscald Outskirt Theme |

## Under Judgement (Requested)
### Sound tracks which weren't selected but were requested or are RC.
| Music | Purpose | Notes |
| ----- | ------- | ----- |
| Peritune Prairies | Piou Islands | Which one? Or pastorale? |
| ~~maoh lastboss3~~ | Moubootaur Final Battle | LICENSE ISSUE - UBP |
| Yggrasil + Wisp / GWriter | Mana Tree Theme | Too far on dev terms |
| ??? | The Maze |
| ??? | Lightbringer Theme |
| ??? | Moubootaur Theme |
| ??? | Story music: Happy Theme | Do we need a happy theme? |
| ??? | Story music: Sad/Thinking Theme | Do we need a thoughtful theme? |
| ??? | Story music: Funny/Casual Theme | Err, not good on the joke side! |
| ??? | Story music: Emergency/Serious Theme | OK |
| Peritune Ominous | Story music: Suspense Theme | Do we need a suspense theme? |
| Peritune Firmament | Aeros Machinery | No sense ingame. Only fav. music? |

----

### Jesusalva Notes
##### You can ignore these. They're too technical.

PS. We might want a server-based way to quickly recover music theme from the map.
Probably by setting $@MAP_DEFAULT_MUSIC$[map_name]=`make maps` and using
getd(getmap()) to retrieve it... Or just... ignore it?

PPS. We might want to allow everyone to use @mymusic on event maps. But I'm not
sure if MZones allow us this sort of control, so these might need to be explicitly
defined in the `OnCall` block, and command made to zero without authorization if
GID < 1 except if on map.

Or > Hardcode 001-* domain as everyone can use @mymusic, but non-sponsors are
restricted to GM tracks: Mythica, original map music, etc.

